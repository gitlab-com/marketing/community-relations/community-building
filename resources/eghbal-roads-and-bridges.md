# Roads and Bridges: The Unseen Labor Behind Our Digital Infrastructure
Nadia Eghbal

## Reading notes and summaries

### Preface

Study concerns the sustainability of open source software:

> "I realized I had walked into a problem with which the producers (open source contributors) were extremely familiar, but that the consumers (software companies and other users of open source software) were seemingly unaware of. That discrepency made me want to look more closely." (p. 6)

Inter-generational communication can be an issue for development: "Today's open source contributor looked very different from an open source contributor ten years ago, much less thirty years ago. And yet these different generations weren't talking to each other, making productive conversations about sustainability difficult." (p. 6)

### History and Background of Digital Infrastructure

Extended discussion of OpenSSL and Heartbleed (ca. 2011).

> "Checking email, reading the news, checking stock prices, shopping online, going to the doctor, calling customer service—whether we realize it or not, everything we do is made possible by projects like OpenSSL." (p. 15)

This is true because reusing tested solutions is cheaper, easier, and "more efficient" (p. 19) than continually re-inventing technologies. "A construction company wouldn't build its hammers and drills from scratch, or source and chop all of the lumber themselves" (p. 19).

More infrastructural metaphors: "This is not unlike a tracking company (Instagram) using a highway (public code) to transport goods for commercial purposes (Instagram's app)" (p. 20).

And continued argument by metaphor: "It is the equivalent of lumberyards, concrete plants and hardware stores donating their raw materials to a construction company, then continuing to support the company's needs" (p. 22).

Rehearsing history of free and open source software movements; preference for "open source" in what remains of this work.

Benefits of open source software:

* Cheaper to build
* Easier to distribute
* Flexible to customize
    - "...enables rapid proliferation of ideas" (p. 35)
* Gives employees more bargaining power
    - "If every company used a proprietary set of tools, [developers] would be less inclined to change jobs"
* Potential to be more stable and secure

### How the Current System Works

We've compared physical and digital infrastructure; however, "there are several major differences [...], which affect how the latter is built and sustained" (p. 38). These are:

* cost
* maintenance
* governance

Open source projects gain support from a many locations, including:

* within a company (Go, React, Swift)
* as a new business (Docker, Npm, Meteor)
* from an individual or community of developers (Python, RubyGems, Twisted)

> "Coordinating international communities of opinionated contributors and managing the expectations of Fortune 500 companies who use your project are challenging tasks for anyone." (p. 52)

So why continue to do the work? What are maintainers' motivations?

* **Building a reputation.** Helps people "prove themselves in public" and gives them "a chance to be part of something big and useful" (p. 53).
* **Feelings of obligation.** Popular projects create dependencies and these dependencies can "make a maintainer feel ethically obligated" (p. 55) to continue working on them.
* **Labor of love.** Developers see the value their work creates for others, the impact they're making, and the positive effect they're having, and they feel motivated to sustain their efforts

### Challenges Facing Digital Infrastructure

Money continues to be a taboo subject in open source and free software movements; "even within open source communities, there is a pervasive belief that money has a corrupting influence on open source" (p. 59).

Projects that exist "on the extremes of size" tend not to experience sustainability issues; "however, many projects are trapped somewhere in the middle: large enough to require significant maintenance, but not quite so large that corporations are clamoring to offer support" (p. 63)

### Sustaining Digital Infrastructure

Chapter explores various support models that have evolved to make opens source digital infrastructure sustainable.

**Bounties:** "Cash prizes for certain development milestones" (p. 91); these "can create perverse incentives for contributing to a project" (p. 91), as what gets funded gets prioritized (and likewise what doesn't get funded gets ignored). It can also create mis-matched priorities when "a company could offer an expensive bounty for a feature that the project owners do not consider important" (p. 91). Tends to incentivize drive-by contributions.

**Services:** Offering forms of value "on top of" or "around" open source software (support, training, certifications, etc.)

**Paid licenses:** Making projects "source-available" or "source visible" rather than "open source" in order to protect the ability to charge for licensing the software 

**Finding a sponsor or donor:** Common approach when charging directly for software would prevent adoption, when managing paid work is prohibitive (or undesirable), and when neutrality and non-commericialization are important (p. 97); the rest of the chapter outlines methods/models of sponsorship or donorship

Models of sponsorship/donorship:

**Crowdfunding:** "Can be useful for funding new features or development work with a clear, tangible outcome" and "help reduce perverse incentives from bounties" (p. 98); still does not address the need for ongoing, sustainable funding

**Corporate sponsorship:** Company hires a contributor to work on a project full-time; these only happen occasionally, "by chance from a sympathetic patron" (p. 103); presents risk of the sponsor exerting outsized influence on a project

Why is digital infrastructure so difficult to fund?

* There's a misperception that this problem has been solved
* There's a lack of cultural understanding and awareness of the problem
* Volunteer culture discourages discussions of money
* Unlike physical infrastructure, digital infrastructure is highly distributed

### Opportunities Ahead

> "Developing effective support strategies requires a nuanced understanding of the open source culture that characterizes so much of our digital infrastructure, as well as recognizing that much has changed in the past five years, including the very definition of 'open source' itself" (p. 125)

We must learn to embrace the concept of *stewardship* rather than *control*.

## Thoughts, impressions, connections

* Pj (async): The tone of all of this seems dire, warning of an impending event if we don't tend to the infrastructure that has been built underneath tech. Especially discussing OpenSSL and heartbleed. I was reminded of the story of [`left=-pad`](https://qz.com/646467/how-one-programmer-broke-the-internet-by-deleting-a-tiny-piece-of-code/)
* Pj: I was caught off guard by the realization that programming languages are open source. MY brain just hadn't made that connection that Python, Rails, all these languages and frameworks are free for literally anyone and I can't conceive of how anyone makes money off of anything. lol. 

Random stuff Pj got caught up on: 
* "When I built my first company starting in 1999 it cost $2.5 million in infrastructure just to get started and another $2.5 million in team costs to code, launch, manage, market, & sell our software. ... Open source became a movement - a mentality. Suddenly infrastructure software was nearly free. WE paid 10% of the normal costs for the software and that money was for software support." (p. 24 - 225)  To see how much industry changed in a seemingly short amount of time makes sense. So far, the book paints this as unsustainable...
* "This type of pragmatism was distinct from the moral obligation that Stallman and his supporters believed they had to promote free software" (p 31) A move from moral purposes, reasoning to a pragmatic one seems to be where we are now; FOSS and OS seem to be a practical from my interactions with the community. "OF course it's open source," they seem to say, "It makes better software/ It gets quicker fixes /" and the moral considerations seem secondary. 
* This is further emphasized with "The open source software movement broke away from the social and political associations with free software by instead focusing on the practical benefits of software development and encouraging wider creative and business applications. As Stallman himself wrote, “Open source is a development methodology; free software is a social movement" (p. 32)

## Discussion prompts

* BB: The author continues to refer to open source software as "public code." But is it truly "public" in the legal sense of the term? Should it be?
* Pj (async): A big part I noticed was this is a recent publication, only six years old, but has anything changed in the six years since this was published? Is Open source more or less worked on these days? How did the pandemic change open source? 
* Pj: More than anything, this made me wonder what exactly Stewardship means in concrete terms: dollars, hours, hosting, support, etc. What specifically do we as a company do to make sure something like OpenSSL doesn't disappear or another heartbleed shows up.
* BB: Does the author seem to conflate a skepticism of market logic(s) with a "pervasive belief that money has a corrupting influence on open source" (p. 59). Take again the extended quotation from DHH, which expresses a concern with the application of "market terms" to free and open source software—which Eghbal calls a warning "against mixing open source with money" (p. 60). Is this really critics' position?

## Discussion notes

* Discussing the primary audience for this book and appreciating the simplicity in the writing for someone who isn't familiar with Open source
* The chasm of consumers than contributors and the privilege of being able to contribute to open source.    
* BB: Hacking Diversity: The Politics of Inclusion in Open Technology Cultures (Princeton Studies in Culture and Technology by Christina Dunbar-Hester is the most sustained interrogation of the issues this white paper describes. 
* FK: The lack of onboarding ramps for new contributors. I am uncomfortable with the idea of "High quality" contributors without pointing out that there aren't high quality on ramps for new contributors to become that.
* The roles and responsibilities of a OSS maintainer - if my project becomes critical infrastructure, what are my responsibilities - ethically ? - to support the project. 
* PJ: Open Source stewardship e.g what is GitLab doing support the ruby project since its part of our infrastructure?
* BB: That's also securing our supply chain and if we're not doing it, it's not the spirit we support with our open core. 
* BB: Perhaps the next section is on business models. Red Hat's business model is a services and support model includes supporting FOSS and that's a huge part of working at Red Hat and a benefit to the ecosystem. The Open Core business model incentivizes a different behavior - you have some features that are open source and others that aren't. You're incentivized to contribute to issues that benefit your model. 
* FK: The services and support business model is interesting. For example, the relationship with Acquia and Drupal in things like contributing back, supporting certain initiatives, and certifications (which can be uncomfortable since they're monetary gain).
* BB: If there was a competitor who forked GitLab, it would lead to some interesting questions about what our value proposition is?
* PJ: The model for our Level Up program is that courses are free and certifications have a cost because the certification is what's more valuable. 
* BB: Coursera might have a similar model. 
* PJ: There are also some Harvard courses online in the same model. 

## Last Updated
2022-11-06
