# Atlas of AI
Kate Crawford

## Reading notes and summaries
<!-- This is where readers stores notes they take as they read—summaries, key quotations, etc. -->

### Introduction

Crawford begins with the story of Clever Hans, a horse trained to compute basic sums, answer questions, etc.

> The story of Clever Hans is compelling from many angles: the relationship between desire, illusion, and action, the business of spectacles, how we anthropomorphize the nonhuman, how biases emerge, and the politics of intelligence.

Central question of this book: "How is intelligence 'made,' and what traps can that create?"

AI is a potent force because it depends on two mythical systems:

> The first myth is that nonhuman systems (be it computers or horses) are analogues for human minds. This perspective assumes that with sufficient training, or enough resources, humanlike intelligence can be created from scratch, without addressing the fundamental ways in which humans are embodied, relational, and set within wider ecologies [...] The second myth is that intelligence is something that exists independently, as though it were natural and distinct from social, cultural, historical, and political forces.

Definitions of AI do more than define objective realities; they construct the boundaries of the debate as they construe it, establish parameters for measuring success:

> Each way of defining artificial intelligence is doing work, setting a frame for how it will be understood, measured, valued, and governed.

Another central argument of the book: AI is neither artificial nor intelligent; instead, AI is embodied, made from material resources and human labor; AI systems are not autonomous or rational; they require training, input, and pre-given rulesets

AI relies on a specific epistemic formation, a specific form(ation) of embodied work and knowledge:

> Computational reason and embodied work are deeply interlinked: AI systems both reflect and produce social relations and understandings of the world.

This is to say, then, that artificial intelligence is unavoidably political, but to understand how this is the case "we need to go beyond neural nets and statistical pattern recognition to instead ask what is being optimized, and for whom, and who gets to decide. Then we can trace the implications of those choices."

Organizing metaphor for the book's methodology is the "atlas," which suggests "that we need new ways to understand the empires of artificial intelligence."

> Artificial intelligence, then, is an idea, an infrastructure, an industry, a form of exercising power, and a way of seeing; it’s also a manifestation of highly organized capital backed by vast systems of extraction and logistics, with supply chains that wrap around the entire planet.

### Chapter 1: Earth

Chapter concerns the "logics of extraction" that underlie modern AI (p. 28); "to understand it all, we need a panoramic view of the planetary scale of computation extraction" (p. 28)

The key resource is lithium, central to battery production:

> Global computation and commerce rely on batteries. The term 'artificial intelligence' may invoke ideas of algorithms, data, and cloud architectures, but none of that can function without the minerals and resources that build computing's core components. (p. 30)

Understanding media as geological "extensions of Earth" is beneficial here:

> Reflecting on media and technology as geological processes enables us to consider the radical depletion of non-renewable resources required to drive the technologies of the present moment. (p. 31)

Glimpsing Ai's supply chains "requires looking for patterns in a global sweep, a sensitivity to the way in which the histories and specific harms are different from place to place and yet are deeply interconnected by the multiple forces of extraction" (p. 38)

> The corporate imaginaries of AI fail to depict the lasting costs and long histories of the materials needed to build computational infrastructures or the energy required to power them. (p. 47)

### Chapter 5: Affect

This chapter investigates the history of efforts to track, classify, and recognize human affective states. This is key for "national security systems and at airports, in education and in hiring startups, from systems that purport to detect psychiatric illness to policing programs that claim to predict violence" (p. 152)

> It holds the promise of reliably filtering friend from foe, distinguishing lies from truths, and using instruments of science to see into interior worlds. (p. 153)

This is different from facial recognition, which aims to identify *particular* faces; the point of reading affect is to determine emotional states that register across *any* face, according to universal categories and responses.

## Thoughts, impressions, connections
<!-- This is where readers record their reactions to the reading, log key connections to other readings or to their own experiences, etc. -->

## Discussion prompts
<!-- This is where readers record statements or questions for synchronous group discussion. -->

## Discussion notes
<!-- This is a space for capturing notes during synchronous discussions. -->

## Last updated
2023-08-23
